const mongoose = require("mongoose");

const ProdutoSchema = new mongoose.Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    brand: { type: String, required: true },
    category: { type: String, required: true },
    originType: { type: String, required: true },
    creationDate: { type: Date, required: true },
    isActive: { type: Boolean, required: true }
});

module.exports = mongoose.model("Produto", ProdutoSchema);