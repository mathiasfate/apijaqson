console.log("server.js rodando!");

const express = require("express");
const app = express();
app.use(express.json());

const port = process.env.PORT || 3001;

require("./server/banco/mongo");
// Usar as rotas
const routes = require('./server/routes/index');
app.use(routes);

app.listen(port, () => {
    return console.log(`api rodando na porta ${port}`);
})