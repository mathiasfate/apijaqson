import React, { useState } from 'react';

function ProdutoForm(props) {
    {
        // Declare variáveis de state
        const handleInputChange = (event) => {
            const { name, value } = event.target
            props.setProduto({ ...props.produto, [name]: value })
            console.log(props);
        }

        const [inputId, setInputId] = useState(false)
        const onClickEditar = () => {
            setInputId(true)
        }


        return (
            <div>
                <form className="form-group">
                    {inputId ? (
                        <div className="form-group">
                            <label>Id</label>
                            <input className="form-control" type="text" name="_id"
                                value={props.produto._id} onChange={handleInputChange} />
                        </div>
                    ) : (
                        <br></br>
                    )}

                    <div className="form-group">
                        <label>title</label>
                        <input className="form-control" type="text" name="title"
                            value={props.produto.title} onChange={handleInputChange} />
                    </div>
                    <div className="form-group">
                        <label>description</label>
                        <input className="form-control" type="text" name="description"
                            value={props.produto.description} onChange={handleInputChange} />
                    </div>
                    <div className="form-group">
                        <label>brand</label>
                        <input className="form-control" type="text" name="brand"
                            value={props.produto.brand} onChange={handleInputChange} />
                    </div>
                    <div className="form-group">
                        <label>category</label>
                        <input className="form-control" type="text" name="category"
                            value={props.produto.category} onChange={handleInputChange} />
                    </div>
                    <div className="form-group">
                        <label>originType</label>
                        <input className="form-control" type="text" name="originType"
                            value={props.produto.originType} onChange={handleInputChange} />
                    </div>
                    <div className="form-group">
                        <button type="button" onClick={props.onClickSalvar}
                            className="btn btn-primary btn-sm">Salvar</button>
                        <button type="button" onClick={props.onClickCancelar}
                            className="btn btn-danger btn-sm">Cancelar</button>
                        <button type="button" onClick={onClickEditar}
                            className="btn btn-success btn-sm">Editar</button>
                    </div>
                </form>


            </div>
        );
    }
}

export default ProdutoForm;